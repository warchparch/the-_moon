# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect, render
from django.contrib import  auth
from django.template import RequestContext
# from django.core.context_processors import csrf
# from django.views.decorators.csrf import c
from django.http import HttpResponse



def login(request):
    args={}
    RequestContext(request).push({'username': 'password'})
    if request.POST:
        username = request.POST.get('username','')
        password = request.POST.get('password','')
        # RequestContext(request).push({'username':'password'})
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request,user)
            return redirect('/')
        else:
            args['login_error'] = "Пользователь не найден"
            # return render_to_response('login.html', args, RequestContext(request))
            return render(request, 'login.html')
    else:
        # return render_to_response('login.html', args, RequestContext(request))
        return render(request, 'login.html')


def logout(request):
    auth.logout(request)
    return redirect('/')
