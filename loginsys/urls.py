from .views import login,logout

from django.conf.urls import url

# app_name = 'auth'
urlpatterns = [
    url(r'^login/$', login),
    url(r'^logout/$', logout),

]