from django.contrib import admin

from .models import Field


# class ChoiceInline(admin.TabularInline):
#     model = Choice
#     extra = 3
#
# class WPackInline(admin.TabularInline):
#     model = Question
#     extra = 1
#
#
# class QuestionAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None,               {'fields': ['question_text']}),
#         ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
#     ]
#     inlines = [ChoiceInline]
#     list_display = ('question_text', 'pub_date', 'was_published_recently')
#     list_filter = ['pub_date']
#     search_fields = ['question_text']
#
# class WPackAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None,               {'fields': ['wpack_text']}),
#         ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
#     ]
#     inlines = [WPackInline]
#     list_display = ('wpack_text', 'pub_date', 'was_published_recently')
#     list_filter = ['pub_date']
#     search_fields = ['wpack_text']
# #
# admin.site.register(Question, QuestionAdmin)
# admin.site.register(WPack, WPackAdmin)
# template_name = 'admin/base.html'