from . import views

from django.conf.urls import url

app_name = 'polls'
urlpatterns = [
    url(r'^$', views.main_page, name='main_page'),
    url(r'^field$', views.field, name='field'),
    url(r'^settings$', views.settings, name='settings'),
    # url(r'^$', views.field, name='field'),
    url(r'^api/get_array/(?P<user_id>\d+)$', views.GetArrayView.as_view(), name='get_array'),
    # url(r'^api/get_array$', views.GetArrayView.as_view(), name='get_array'),
    url(r'^api/save_array/$', views.SaveArrayView.as_view(), name='save_array'),
    # url(r'^wpack/$',views.WPacksView.as_view(),name='wpacks'),
    # url(r'^wpack/(?P<pk>[0-9]+)/$', views.wpack, name='wpack'),
    # url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    # url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    # url(r'^../auth/logout/$', 'loginsys.views.logout'),
    # url(r'^wpack/[0-9]/auth/logout/$', 'loginsys.views.logout'),
    # url(r'^[0-9]+/auth/logout/$', 'loginsys.views.logout'),
    # url(r'^test/$', views.test, name='test'),

]