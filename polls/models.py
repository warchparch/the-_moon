import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db.models.signals import post_save
from django.dispatch import receiver



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #базовые характеристики
    health = models.IntegerField(default=50)
    d_honor = models.IntegerField(default=50)
    rb_honor = models.IntegerField(default=50)
    sm_honor = models.IntegerField(default=50)
    team_honor = models.IntegerField(default=50)
    tupik_honor = models.IntegerField(default=50)
    money = models.IntegerField(default=1000)
    day = models.IntegerField(default=1)
    work = models.IntegerField(default=50)
    #прирост характеристики за последний день
    last_health = models.IntegerField(default=0)
    last_d_honor = models.IntegerField(default=0)
    last_rb_honor = models.IntegerField(default=0)
    last_sm_honor = models.IntegerField(default=0)
    last_team_honor = models.IntegerField(default=0)
    last_tupik_honor = models.IntegerField(default=0)
    last_money = models.IntegerField(default=0)
    last_work = models.IntegerField(default=0)
    field = JSONField(default={})





class Field(models.Model):

    # field_db = models.CharField(default='', max_length=99999)
    rover = models.IntegerField(default=4)
    sand1 = models.IntegerField(default=1)
    sand2 = models.IntegerField(default=1)
    sand3 = models.IntegerField(default=1)
    sand4 = models.IntegerField(default=1)
    crater1 = models.IntegerField(default=1)
    crater2 = models.IntegerField(default=1)
    crater3 = models.IntegerField(default=1)
    crater4 = models.IntegerField(default=1)
    pointer1 = models.IntegerField(default=3)
    pointer1d = models.IntegerField(default=2)
    pointer2 = models.IntegerField(default=2)
    pointer21 = models.IntegerField(default=2)
    pointer2d = models.IntegerField(default=2)
    pointer4 = models.IntegerField(default=3)
    pointer41 = models.IntegerField(default=2)
    pointer4d = models.IntegerField(default=2)
    pointer6 = models.IntegerField(default=2)
    crystal1 = models.IntegerField(default=5)
    crystal2 = models.IntegerField(default=6)
    crystal3 = models.IntegerField(default=4)
    crystal4 = models.IntegerField(default=1)
    crystal5 = models.IntegerField(default=1)
    meteor = models.IntegerField(default=4)
    plague = models.IntegerField(default=4)
    radiation = models.IntegerField(default=4)
    void = models.IntegerField(default=5)
    magnet = models.IntegerField(default=4)
    password = models.IntegerField(default=4)
    tornado = models.IntegerField(default=4)
    sphinx = models.IntegerField(default=4)
    grasshopper = models.IntegerField(default=2)
    canon = models.IntegerField(default=2)
    deadend = models.IntegerField(default=4)
    doom = models.IntegerField(default=1)
    spider = models.IntegerField(default=1)
    skull = models.IntegerField(default=1)
    magican = models.IntegerField(default=1)
    castle = models.IntegerField(default=1)
    evil = models.IntegerField(default=1)
    cat = models.IntegerField(default=1)
    benter = models.IntegerField(default=1)
    bexit = models.IntegerField(default=1)
    poison = models.IntegerField(default=1)




@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


