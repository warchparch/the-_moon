/**
 * Created by ahtoh on 08.04.17.
 */


//CSRF
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var pl1crd;


//main
$(document).ready(function () {

    // $('#field').append('<img  class="player" src ="{% static "polls/images/misc/player1.jpg" %}"/>');
    $('.playerT').draggable();
    $('.player').draggable(
        {
            // containment:'parent',
            // revert:true,
        opacity: .4,


        create: function(){
            $(this).data('position',$(this).position());
            // $(this).animate({top:$(this).position(0).top,left:100},{duration:3000,easing:'easeOutBack'});
            // console.log($(this).position())

        },
        cursorAt:{left:15},

        cursor:'move',
        start:function(){
            // console.log('dragging started');
            // highlight_passable(pl1crd);
            $(this).stop(true,true)

        }
   }
    );
    // Создание привязки по центру поля
    $('.cover,.r0,.r90,.r180,.r270,.r360').droppable({
        drop:function(event,ui){snapToMiddle(ui.draggable,$(this));},
        over:function(event,ui){greenLight(ui.draggable,$(this));},
        out:function(event,ui){offLight(ui.draggable,$(this));},

    });

    $('.cover,.r0,.r90,.r180,.r270,.r360').click(function () {
        // $(this).fadeOut('fast');
        var x = $(this).data('id');
        getArray(function (data) {
    // console.log(data.field_code[x].faced);
    // console.log(data.field_code[x]['crd']);
            main_array = data.field_code[x];
            // console.log(data.field_code[x]['col'])
            console.log(main_array['move_to'])

});
    });
});

//Центрирует фишку
function snapToMiddle(dragger, target) {
    // $(".f_tile").append(dragger);
    var topMove = target.position().top + (target.outerHeight(true) - dragger.outerHeight(true)) / 2;
    var leftMove = target.position().left + (target.outerWidth(true) - dragger.outerWidth(true)) / 2;
    // var leftMove= target.position().left - dragger.data('position').left  + (target.outerWidth(true) - dragger.outerWidth(true)) / 2;
    dragger.animate({top: topMove, left: leftMove}, {duration: 300, easing: 'easeOutBack'});
    // dragger.appendTo($('.f_tile'));
    console.log(target.offset());
    $('#event_log').prepend('<div><p>' + dragger.data('id') + ' переместил фишку на поле ' + target.data('id') + '</p></div>');
    passable(target.data('id'));
    // window.pl1crd = target.data('id');
    // console.log('player1crd: ', pl1crd);


    if (target.context.id === 'cover') {
        target.fadeOut('fast');
        // $( dragger).draggable( "option", "revert", false );
        getArray(function (data) {
            data.field_code[target.data("id")]["faced"] = 1;
            writeArray(data.field_code);

            // console.log(data.field_code[target.data("id")]["faced"])

            //     console.log(main_array['row'])
            // console.log(target.data("id"))
        // $( dragger).draggable( "option", "revert", true );
        })
        // $('#event_log').append("<div><p>{{dragger.context.id}}</p></div>");


    }
}

function greenLight(dragger, target){
     target.animate({borderColor: "#1bff1b"}, 500)
     }
//
function offLight(dragger, target){
    target.animate({borderColor: "transparent"}, 500)
    }

//возвращет фишку назад, если условия невыполнимы
// function returnBack(dragger, target){
//     var topMove = dragger.position().top;
//     var leftMove= dragger.position().left;
//     dragger.animate({top:topMove,left:leftMove},{duration:300,easing:'easeOutBack'});
//
// }

var hue;
//вызов массива из БД
function getArray(callback) {
    var $ret = 0;
    $.ajax({
        type: 'GET',
        url: '/api/get_array/4',
        dataType: 'JSON',
        // cache: false,
        success: function (data) {
            callback(data);
        }

    });
    return $ret;
}

//возвращение измененного массива в БД
function writeArray(changed_array) {
    // changed_array = changed_array['a1'];
    // changed_array = '["foo", {"bar":["baz", null, 1.0, 2]}]';
    console.log(changed_array);
    // changed_array = 789;
    $.ajax({
        type: "POST",
        url: '/api/save_array/',
        // dataType:'JSON',
        data: {
            array: JSON.stringify(changed_array)
        },
        success: function (response) {
            if (response.status === "OK") {
                console.log('All is fine')
            }
            else {
                console.log('All is not fine')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Doesn't work")

        }
    });

}
//finding tiles possible to move at
function passable(coordinate){
            getArray(function (data) {
                col = data.field_code[coordinate]["col"];
                row = data.field_code[coordinate]["row"];
            var passable_list = [];
                for (field in data.field_code){
                    if (((Math.abs(data.field_code[field]['col'] - col) <= 1)
                        &&(Math.abs(data.field_code[field]['row'] - row) <= 1))
                        && !((Math.abs(data.field_code[field]['col'] - col)=== 0)
                        &&(Math.abs(data.field_code[field]['row'] - row) === 0)))
                    {
                        if((data.field_code[field]['move_to']).length  > 0){
                            console.log('coord: ',data.field_code[field]['crd']);
                            passable_list = passable_list.concat(expand(data.field_code))
                        }
                        passable_list.push(data.field_code[field]['crd'])

                    }

                }
                console.log(Unique(passable_list))
            });
}
//finding coordinate by row and column
function find_coordinate(row,col,field_code) {
    var coordinate;
    for (field2 in field_code) {
        if ((field_code[field2]['col'] === row) && (field_code[field2]['row'] === col)) {
            coordinate = field_code[field2]['crd']
        }
    }
    return coordinate
}

//creating list of unique elements
function Unique(A)
{
    var n = A.length, k = 0, B = [];
    for (var i = 0; i < n; i++)
     { var j = 0;
       while (j < k && B[j] !== A[i]) j++;
       if (j == k) B[k++] = A[i];
     }
    return B;
}

//adding pointed tiles to possible
function expand(field_code) {
    var expanding_list =[];
    for (var item=0; item < (field_code[field]['move_to']).length;item++){
        switch(field_code[field]['move_to'][item]){
            case 0:
                expanding_list.push(find_coordinate(field_code[field]['col'],
                    field_code[field]['row']+1,field_code));
                break;
            case 2:
                expanding_list.push(find_coordinate(field_code[field]['col']+1,
                    field_code[field]['row'],field_code));
                break;
            case 1:
                expanding_list.push(find_coordinate(field_code[field]['col']+1,
                    field_code[field]['row']+1,field_code));
                break;
            case 3:
                expanding_list.push(find_coordinate(field_code[field]['col']+1,
                    field_code[field]['row']-1,field_code));
                break;
            case 4:
                expanding_list.push(find_coordinate(field_code[field]['col'],
                    field_code[field]['row']-1,field_code));
                break;
            case 5:
                expanding_list.push(find_coordinate(field_code[field]['col']-1,
                    field_code[field]['row']-1,field_code));
                break;
            case 6:
                expanding_list.push(find_coordinate(field_code[field]['col']-1,
                    field_code[field]['row'],field_code));
                break;
            case 7:
                expanding_list.push(find_coordinate(field_code[field]['col']-1,
                    field_code[field]['row']+1,field_code));
                break;
            default:
                console.log('default');
                break;

        }
    }
    for (item in expanding_list){
        console.log('item: ',expanding_list[item]);
        if((field_code[expanding_list[item]]['move_to']).length  > 0){
            // expanding_list = expanding_list.concat(expand(field_code));
            console.log('not the end')
        }
    }
    return expanding_list
}
// function highlight_passable(crd){
//     crd = 'a1'
//     list_of = passable(crd)
//     console.log('list of:', list_of)
// }