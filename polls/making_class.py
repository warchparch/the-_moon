from random import randint
from copy import deepcopy


class Field():
    space = 2
    rover = 3
    sand1 = 4
    pointer1 = 5


# def determ_tile(name='space', faced=False, hold_by_a_team_member=0, contains_crystals=0, permanent=True,
#                 contains_special=False, passable=True, in_space=False, turns_to_wait=0,
#                 can_be_final=True, rotate=0):
#     return ({'name': name, 'faced': faced, "hold_by_a_team_member": hold_by_a_team_member,
#              'contains_crystals': contains_crystals,
#              'permanent': permanent, 'contains_special': contains_special, 'passable': passable,
#              'in_space': in_space,
#              'turns_to_wait': turns_to_wait, 'can_be_final': can_be_final, 'rotate': rotate})
def determ_tile(name='space', rotate=0):
    return ({'name': name, 'rotate': rotate})


y = {'space': determ_tile('space'), 'rover': determ_tile('rover'), 'sand1': determ_tile('sand1'),
     'sand2': determ_tile('sand2'), 'sand3': determ_tile('sand3'), 'sand4': determ_tile('sand4'),
     'crater1': determ_tile('crater1'), 'crater2': determ_tile('crater2'), 'crater3': determ_tile('crater3'),
     'crater4': determ_tile('crater4'), 'pointer1': determ_tile('pointer1', rotate=1),
     'pointer1d': determ_tile('pointer1d', rotate=1), 'pointer2': determ_tile('pointer2', rotate=1),
     'pointer21': determ_tile('pointer21', rotate=1), 'pointer2d': determ_tile('pointer2d', rotate=1),
     'pointer4': determ_tile('pointer4'), 'pointer41': determ_tile('pointer41'), 'pointer4d': determ_tile('pointer4d'),
     'pointer6': determ_tile('pointer6', rotate=1), 'crystal1': determ_tile('crystal1'),
     'crystal2': determ_tile('crystal2'), 'crystal3': determ_tile('crystal3'), 'crystal4': determ_tile('crystal4'),
     'crystal5': determ_tile('crystal5'), 'meteor': determ_tile('meteor'), 'plague': determ_tile('plague'),
     'radiation': determ_tile('radiation'), 'void': determ_tile('void'), 'magnet': determ_tile('magnet'),
     'password': determ_tile('password'), 'tornado': determ_tile('tornado', rotate=1), 'sphinx': determ_tile('sphinx'),
     'grasshopper': determ_tile('grasshopper'), 'canon': determ_tile('canon', rotate=1),
     'deadend': determ_tile('deadend'), 'doom': determ_tile('doom'), 'spider': determ_tile('spider'),
     'skull': determ_tile('skull'), 'magican': determ_tile('magican'), 'castle': determ_tile('castle'),
     'evil': determ_tile('evil'), 'cat': determ_tile('cat'), 'benter': determ_tile('benter'),
     'bexit': determ_tile('bexit'), 'poison': determ_tile('poison')}

z = []
for i in y:
    for j in range(getattr(Field, i)):
        z.append(y[i])
z = [deepcopy(x) for x in z]
# z = deepcopy(z)
# print(z)
p = []
for i in z:
    if i['rotate'] == 1:
        print("HUE")
        # i['rotate'] = deepcopy(i['rotate'])
        i['rotate'] = randint(1, 999)

h = 0
for i in z:
    p.append({str(h): i})
    h += 1
print(p)
# print(getattr(Field,'space'))
