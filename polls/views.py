from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views import generic
from random import randint, shuffle
from .models import Field
from copy import deepcopy
from django.views.decorators.csrf import csrf_exempt
import json

def make_short_list(field_array):
    short_list = []
    for col in range(1, 13):
        for row in range(1, 13):
            for tile in field_array:
                if (field_array[tile]['row'] == (row)) and (field_array[tile]['col'] == (col)):
                    short_list.append(field_array[tile])
    field_array = short_list
    x = []
    for i in range(12):
        x += [field_array[i * 12:i * 12 + 12][::-1]]
    return x


def field(request):
    if request.user.is_authenticated:
        field_array = request.user.profile.field
        field_code = make_short_list(field_array)

        return render(request, 'field.html', {'field_code': field_code}, )
    else:
        return render(request, 'field.html')

def main_page(request):
    return render(request, 'main_page.html', )


def settings(request):
    def determ_tile(name='space', rotate=0, col = 0, row = 0,
                    faced = 0, crd = '',move_to =[],perm=1):
        return ({'name': name, 'rotate': rotate,'col':col,'row':row,
                 'faced':faced,'crd':crd, "move_to":move_to,'perm':perm})

    tile_property = { 'rover': determ_tile('rover', perm=0),
                      'sand1': determ_tile('sand1'),
                      'sand2': determ_tile('sand2'),
                      'sand3': determ_tile('sand3'),
                      'sand4': determ_tile('sand4'),
                      'crater1': determ_tile('crater1'),
                      'crater2': determ_tile('crater2'),
                      'crater3': determ_tile('crater3'),
                      'crater4': determ_tile('crater4'),
                      'pointer1': determ_tile('pointer1', rotate=1, move_to=[0]),
                      'pointer1d': determ_tile('pointer1d', rotate=1, move_to=[1]),
                      'pointer2': determ_tile('pointer2', rotate=1, move_to=[0,4]),
                      'pointer21': determ_tile('pointer21', rotate=1, move_to=[0,2]),
                      'pointer2d': determ_tile('pointer2d', rotate=1, move_to=[1,5]),
                      'pointer4': determ_tile('pointer4', move_to=[0, 2, 4, 6]),
                      'pointer41': determ_tile('pointer41', move_to=[0, 2, 4, 6]),
                      'pointer4d': determ_tile('pointer4d', move_to=[1, 3, 5, 7]),
                      'pointer6': determ_tile('pointer6', rotate=1, move_to=[0, 1, 2, 4, 5, 6]),
                      'crystal1': determ_tile('crystal1'),
                      'crystal2': determ_tile('crystal2'),
                      'crystal3': determ_tile('crystal3'),
                      'crystal4': determ_tile('crystal4'),
                      'crystal5': determ_tile('crystal5'),
                      'meteor': determ_tile('meteor'),
                      'plague': determ_tile('plague'),
                      'radiation': determ_tile('radiation'),
                      'void': determ_tile('void'),
                      'magnet': determ_tile('magnet'),
                      'password': determ_tile('password', perm=0),
                      'tornado': determ_tile('tornado', rotate=1),
                      'sphinx': determ_tile('sphinx', perm=0),
                      'grasshopper': determ_tile('grasshopper'),
                      'canon': determ_tile('canon', rotate=1),
                      'deadend': determ_tile('deadend'),
                      'doom': determ_tile('doom', perm=0),
                      'spider': determ_tile('spider'),
                      'skull': determ_tile('skull', perm=0),
                      'magican': determ_tile('magican', perm=0),
                      'castle': determ_tile('castle'),
                      'evil': determ_tile('evil', perm=0),
                      'cat': determ_tile('cat', perm=0),
                      'benter': determ_tile('benter'),
                      'bexit': determ_tile('bexit'),
                      'poison': determ_tile('poison', perm=0)}

    field_array = []
    for tile in tile_property:
        for iterator in range(getattr(Field(), tile)):
            field_array.append(tile_property[tile])
    field_array = [deepcopy(tile) for tile in field_array]
    # Поворот фишки
    for tile in field_array:
        if tile['rotate'] == 1:
            randomiser = randint(1, 4)
            tile['rotate'] = randomiser*90
            # поворот значения стрелок
            for item in range(len(tile['move_to'])):
                tile['move_to'][item] += 2*(randomiser)
                if tile['move_to'][item] > 7:
                    tile['move_to'][item] -= 8
            tile['move_to'] = sorted(tile['move_to'])
    shuffle(field_array)
    counter = 0
    counter2 = 12
    array = {}
    a_list = ['b','c','d','e','f','g','h','i','j','k',]
    for row in a_list[::-1]:
        counter2 -= 1
        for col in range(2, 12):
            field_array[counter]['row'] = col
            field_array[counter]['col'] = counter2
            array[row + str(col)] = field_array[counter]
            counter += 1
    field_array = array
    for iterator in range(1,13):
        field_array['a' + str(iterator)] = determ_tile('space', col=1, row=iterator, faced=1)
        field_array['l' + str(iterator)] = determ_tile('space', col=12, row=iterator, faced=1)
    counter = 2
    for iterator in a_list:
        field_array[iterator + str(1)] = determ_tile('space', row=1, col=counter, faced=1)
        field_array[iterator + str(12)] = determ_tile('space', row=12, col=counter, faced=1)
        counter += 1
    for iterator in field_array:
        field_array[iterator]['crd'] = iterator

    request.user.profile.field = json.dumps(field_array)
    request.user.profile.field = field_array
    request.user.profile.save()
    field_code = make_short_list(field_array)
    return render(request, 'settings.html', {'field_code': field_code}, )


class GetArrayView(generic.View):
    def get(self, request, user_id):
        if request.user.is_authenticated:
            field_code = request.user.profile.field
        return JsonResponse({'field_code': field_code})


class SaveArrayView(generic.View):
    def post(self, request):
        post = request.POST
        changed_array = json.loads(post.get('array'))
        print(changed_array)
        request.user.profile.field = changed_array
        request.user.profile.save()
        return JsonResponse({'status': 'OK'})
