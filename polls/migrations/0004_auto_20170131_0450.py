# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_question_question_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='qpack',
            field=models.ForeignKey(to='polls.QPack'),
        ),
    ]
