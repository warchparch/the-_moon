# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20170124_0832'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='question_logo',
            field=models.CharField(default=1, max_length=1000),
        ),
    ]
