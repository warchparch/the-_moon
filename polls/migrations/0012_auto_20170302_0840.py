# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0011_auto_20170302_0632'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='last_d_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_health',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_money',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_rb_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_sm_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_team_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_tupik_honor',
            field=models.IntegerField(default=0),
        ),
    ]
