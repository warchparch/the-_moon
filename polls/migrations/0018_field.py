# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0017_profile_field'),
    ]

    operations = [
        migrations.CreateModel(
            name='Field',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('rover', models.IntegerField(default=4)),
                ('sand1', models.IntegerField(default=1)),
                ('sand2', models.IntegerField(default=1)),
                ('sand3', models.IntegerField(default=1)),
                ('sand4', models.IntegerField(default=1)),
                ('crater1', models.IntegerField(default=1)),
                ('crater2', models.IntegerField(default=1)),
                ('crater3', models.IntegerField(default=1)),
                ('crater4', models.IntegerField(default=1)),
                ('pointer1', models.IntegerField(default=3)),
                ('pointer1d', models.IntegerField(default=2)),
                ('pointer2', models.IntegerField(default=2)),
                ('pointer21', models.IntegerField(default=2)),
                ('pointer2d', models.IntegerField(default=2)),
                ('pointer4', models.IntegerField(default=3)),
                ('pointer41', models.IntegerField(default=2)),
                ('pointer4d', models.IntegerField(default=2)),
                ('pointer6', models.IntegerField(default=2)),
                ('crystal1', models.IntegerField(default=5)),
                ('crystal2', models.IntegerField(default=6)),
                ('crystal3', models.IntegerField(default=4)),
                ('crystal4', models.IntegerField(default=1)),
                ('crystal5', models.IntegerField(default=1)),
                ('meteor', models.IntegerField(default=4)),
                ('plague', models.IntegerField(default=4)),
                ('radiation', models.IntegerField(default=4)),
                ('void', models.IntegerField(default=5)),
                ('magnet', models.IntegerField(default=4)),
                ('password', models.IntegerField(default=4)),
                ('tornado', models.IntegerField(default=4)),
                ('sphinx', models.IntegerField(default=4)),
                ('grasshopper', models.IntegerField(default=2)),
                ('canon', models.IntegerField(default=2)),
                ('deadend', models.IntegerField(default=4)),
                ('doom', models.IntegerField(default=1)),
                ('spider', models.IntegerField(default=1)),
                ('skull', models.IntegerField(default=1)),
                ('magican', models.IntegerField(default=1)),
                ('castle', models.IntegerField(default=1)),
                ('evil', models.IntegerField(default=1)),
                ('cat', models.IntegerField(default=1)),
                ('benter', models.IntegerField(default=1)),
                ('bexit', models.IntegerField(default=1)),
                ('poison', models.IntegerField(default=1)),
            ],
        ),
    ]
