# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='QPack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('qpack_text', models.CharField(max_length=200)),
            ],
        ),
        migrations.AlterField(
            model_name='question',
            name='question_text',
            field=models.CharField(help_text='Please use the following format: <em>YYYY-MM-DD</em>.', max_length=200),
        ),
        migrations.AddField(
            model_name='question',
            name='qpack',
            field=models.ForeignKey(default=0, to='polls.QPack'),
        ),
    ]
