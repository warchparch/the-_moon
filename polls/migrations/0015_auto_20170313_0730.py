# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0014_auto_20170309_0528'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='inc_work',
            field=models.IntegerField(default=0),
        ),
    ]
