# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0012_auto_20170302_0840'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='inc_work',
            field=models.IntegerField(default=50),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_work',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='work',
            field=models.IntegerField(default=50),
        ),
    ]
