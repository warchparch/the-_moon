# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0010_auto_20170301_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='inc_rb_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='rb_honor',
            field=models.IntegerField(default=50),
        ),
        migrations.AddField(
            model_name='question',
            name='max_rb_honor',
            field=models.IntegerField(default=100),
        ),
        migrations.AddField(
            model_name='question',
            name='min_rb_honor',
            field=models.IntegerField(default=0),
        ),
    ]
