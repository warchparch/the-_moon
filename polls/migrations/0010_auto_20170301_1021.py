# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0009_auto_20170301_0927'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='inc_d_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='inc_health',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='inc_money',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='inc_sm_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='inc_team_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='inc_tupik_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='profile',
            name='d_honor',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='health',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='sm_honor',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='team_honor',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='profile',
            name='tupik_honor',
            field=models.IntegerField(default=50),
        ),
        migrations.AlterField(
            model_name='question',
            name='max_d_honor',
            field=models.IntegerField(default=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='max_health',
            field=models.IntegerField(default=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='max_sm_honor',
            field=models.IntegerField(default=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='max_team_honor',
            field=models.IntegerField(default=100),
        ),
        migrations.AlterField(
            model_name='question',
            name='max_tupik_honor',
            field=models.IntegerField(default=100),
        ),
    ]
