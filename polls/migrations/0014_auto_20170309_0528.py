# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0013_auto_20170302_0926'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='req_d_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='req_money',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='req_rb_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='req_sm_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='req_team_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='choice',
            name='req_tupik_honor',
            field=models.IntegerField(default=0),
        ),
    ]
