# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0004_auto_20170131_0450'),
    ]

    operations = [
        migrations.CreateModel(
            name='WPack',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('wpack_text', models.CharField(max_length=200)),
                ('pub_date', models.DateTimeField(blank=True, verbose_name='date_published')),
            ],
        ),
        migrations.RemoveField(
            model_name='question',
            name='qpack',
        ),
        migrations.DeleteModel(
            name='QPack',
        ),
        migrations.AddField(
            model_name='question',
            name='wpack',
            field=models.ForeignKey(to='polls.WPack', default=0),
            preserve_default=False,
        ),
    ]
