# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0008_auto_20170301_0926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='max_tupik_honor',
            field=models.IntegerField(default=10),
        ),
    ]
