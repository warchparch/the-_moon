# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0016_test'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='field',
            field=models.CharField(default='0', max_length=1000),
        ),
    ]
