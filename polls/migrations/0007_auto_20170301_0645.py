# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0006_profile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='bio',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='birth_date',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='location',
        ),
        migrations.AddField(
            model_name='profile',
            name='d_honor',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='profile',
            name='health',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='profile',
            name='money',
            field=models.IntegerField(default=1000),
        ),
        migrations.AddField(
            model_name='profile',
            name='sm_honor',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='profile',
            name='team_honor',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='profile',
            name='tupik_honor',
            field=models.IntegerField(default=5),
        ),
    ]
