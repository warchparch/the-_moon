# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0007_auto_20170301_0645'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='day',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='question',
            name='max_d_honor',
            field=models.IntegerField(default=10),
        ),
        migrations.AddField(
            model_name='question',
            name='max_day',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='question',
            name='max_health',
            field=models.IntegerField(default=10),
        ),
        migrations.AddField(
            model_name='question',
            name='max_money',
            field=models.IntegerField(default=10000),
        ),
        migrations.AddField(
            model_name='question',
            name='max_sm_honor',
            field=models.IntegerField(default=10),
        ),
        migrations.AddField(
            model_name='question',
            name='max_team_honor',
            field=models.IntegerField(default=10),
        ),
        migrations.AddField(
            model_name='question',
            name='max_tupik_honor',
            field=models.IntegerField(default=5),
        ),
        migrations.AddField(
            model_name='question',
            name='min_d_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='question',
            name='min_day',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='question',
            name='min_health',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='question',
            name='min_money',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='question',
            name='min_sm_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='question',
            name='min_team_honor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='question',
            name='min_tupik_honor',
            field=models.IntegerField(default=0),
        ),
    ]
