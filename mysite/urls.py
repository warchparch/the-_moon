from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^polls/', include('polls.urls', namespace='polls')),
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('loginsys.urls', namespace='login')),
    url(r'^', include('polls.urls', namespace='main'))
]